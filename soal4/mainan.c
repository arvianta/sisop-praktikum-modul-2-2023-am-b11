#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

// Danno Denis Dhaifullah 5025211027 && Rayhan Arvianta Bayuputra 5025211217

void run_script(int hr, int min, int sec, char* script_path)
{
    time_t current_time;
    struct tm *local_time;

    current_time = time(NULL);
    local_time = localtime(&current_time);

    if ((hr == -1 || hr == local_time->tm_hour) && (min == -1 || min == local_time->tm_min) && (sec == -1 || sec == local_time->tm_sec))
    {
        pid_t pid = fork();
        if (pid == 0)
        {
            char *args[] = {"bash", script_path, NULL};
            execvp("bash", args);

        }
    }
}

int main(int argc, char *argv[])
{
    if (argc != 5) {
        fprintf(stderr, "Error: Expected 5 arguments, received %d\n", argc);
        exit(EXIT_FAILURE);
    }

    int hr, min, sec;

    if (strcmp(argv[1], "*") == 0) {
        hr = -1;
    } else {
        hr = atoi(argv[1]);
        if (hr < 0 || hr > 23) {
            fprintf(stderr, "Error: Hour argument '%s' must be an integer between 0 and 23\n", argv[1]);
            exit(EXIT_FAILURE);
        }
    }

    if (strcmp(argv[2], "*") == 0) {
        min = -1;
    } else {
        min = atoi(argv[2]);
        if (min < 0 || min > 59) {
            fprintf(stderr, "Error: Minute argument '%s' must be an integer between 0 and 59\n", argv[2]);
            exit(EXIT_FAILURE);
        }
    }

    if (strcmp(argv[3], "*") == 0) {
        sec = -1;
    } else {
        sec = atoi(argv[3]);
        if (sec < 0 || sec > 59) {
            fprintf(stderr, "Error: Second argument '%s' must be an integer between 0 and 59\n", argv[3]);
            exit(EXIT_FAILURE);
        }
    }

    if (access(argv[4], F_OK) == -1) {
        fprintf(stderr, "Error: Script file '%s' does not exist\n", argv[4]);
        exit(EXIT_FAILURE);
    } else if (access(argv[4], X_OK) == -1) {
        fprintf(stderr, "Error: Script file '%s' is not executable\n", argv[4]);
        exit(EXIT_FAILURE);
    }

    pid_t pid = fork();
    if (pid < 0)
    {
        printf("Error: Fork failed.\n");
        exit(EXIT_FAILURE);
    }
    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    pid_t sid = setsid();
    if (sid < 0)
    {
        printf("Error: Could not create new session.\n");
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0)
    {
        printf("Error: Could not change working directory to root.\n");
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1)
    {
        run_script(hr, min, sec, argv[4]);

        sleep(1);
    }

    return 0;
}

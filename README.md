# Praktikum Sistem Operasi Modul 2
Anggota Kelompok ``B11`` 
| Nama                      | NRP        |
|---------------------------|------------|
| Rayhan Arvianta Bayuputra | 5025211217 |
| Danno Denis Dhaifullah    | 5025211027 |
| Samuel Berkat Hulu        | 5025201055 |



## Nomor 1
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq

a. Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.

b. Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

c. Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

d. Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.


### Soal A
**Solusi :**
```c
pid = fork();

    if (pid < 0) {
        perror("fork");
    }
    if(pid == 0) {
        execlp("wget", "wget", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL);
        exit(0);
    } else {
        // unzip
        waitpid(pid, &status, 0);
        pid = fork();
        
        if (pid < 0) {
            perror("fork");
        }
        if (pid == 0) {
            execlp("unzip", "unzip", "binatang.zip", NULL);
        } else {
            ...
```
**Penjelasan Kode :**
- Dilakukan fork untuk membuat child process, dimana child process pertama melakukan download menggunakan fungsi ``execlp`` yang didalamnya terdapat perintah ``wget`` untuk mendownload file dari url yang telah diberikan. File yang didownload akan distore pada ``binatang.zip``. 
- Setelah mendownload, untuk melakukan unzip nantinya parent akan membuat child proses lagi, namun harus menunggu hingga proses download selesai menggunakan ``waitpid``. Setelah itu melakukan unzip menggunakan fungsi ``execlp``.

### Soal B 
**Solusi :**
```c
waitpid(pid, &status, 0);
            pid = fork();

            if (pid < 0) {
                perror("fork");
            }

            if (pid == 0) {
                randomSelect();
            }
            else {
                ...
```
**Penjelasan Kode :**
- Parent process akan menunggu child process sebelumnya untuk selesai, yaitu proses unzip, kemudian akan membuat child process baru yang ditugaskan untuk melakukan random select dari binatang-binatang yang ada.

```c
void randomSelect() {
    char *picture_folder = "/home/arvianta/sisop/P2/NO1";
    char command[1000];
    sprintf(command, "/bin/bash -c '"
                    "picture_folder=\"%s\" && "
                    "picture_files=($picture_folder/*.jpg) && "
                    "if [ ${#picture_files[@]} -eq 0 ]; then "
                    "echo \"No .jpg files found in the folder\" && exit 1; "
                    "fi && "
                    "random_index=$((RANDOM %% ${#picture_files[@]})) && "
                    "random_picture=\"$(basename ${picture_files[$random_index]} .jpg)\" && "
                    "echo \"Pada shift kali ini, Grape-Kun menjaga $random_picture\"'"
                    , picture_folder);
    system(command);
}

```
**Penjelasan Kode :**
- Pada fungsi ``randomSelect()``, dilakukan pemilihan nama file binatang secara random dengan menggunakan fungsi ``system`` yang di dalamnya terdapat script untuk melakukan hal tersebut. Pada script, pertama dilakukan pengecekan apabila ``picture_files`` terdapat pada direktori tersebut atau tidak. Setelah melakukan pengecekan, dilakukan pengambilan nama file secara random dan diassign ke ``random_index``. Kemudian nama file diextract dan disimpan pada ``random_picture``. Setelah mendapatkan nama file randomnya, hasilnya di print menggunakan ``echo``.

### Soal C 
**Solusi :**
```c
waitpid(pid, &status, 0);
                pid = fork();

                if (pid < 0) {
                    perror("fork");
                }

                if (pid == 0) {
                    movePictures();
                }

                else {
                    ...
```
**Penjelasan Kode :**
- Parent process akan menunggu child process sebelumnya untuk selesai, yaitu proses random select, kemudian akan membuat child process baru yang ditugaskan untuk melakukan pemindahan file ke direktori baru sesuai dengan tempat tinggal dari binatangnya.

```c
void movePictures() {
    char *picture_folder = "/home/arvianta/sisop/P2/NO1";
    char command[2000];
    sprintf(command, "/bin/bash -c '"
            "# get the picture folder from the first argument\n"
            "picture_folder=\"%s\"\n"
            "\n"
            "# create directories for each type of animal\n"
            "mkdir -p \"$picture_folder/HewanDarat\"\n"
            "mkdir -p \"$picture_folder/HewanAmphibi\"\n"
            "mkdir -p \"$picture_folder/HewanAir\"\n"
            "\n"
            "# loop through picture files in folder\n"
            "for file in \"$picture_folder\"/*; do\n"
            "  # get the filename without path and extension\n"
            "  filename=$(basename -- \"$file\")\n"
            "  extension=\"${filename##*.}\"\n"
            "  filename=\"${filename%%.*}\"\n"
            "  \n"
            "  # get the last name of the animal\n"
            "  last_name=\"${filename##*_}\"\n"
            "  \n"
            "  # move the file to the appropriate directory based on last name\n"
            "  if [[ \"$last_name\" == \"darat\" ]]; then\n"
            "    mv \"$file\" \"$picture_folder/HewanDarat/$filename.$extension\"\n"
            "  elif [[ \"$last_name\" == \"amphibi\" ]]; then\n"
            "    mv \"$file\" \"$picture_folder/HewanAmphibi/$filename.$extension\"\n"
            "  elif [[ \"$last_name\" == \"air\" ]]; then\n"
            "    mv \"$file\" \"$picture_folder/HewanAir/$filename.$extension\"\n"
            "  fi\n"
            "done'"
            , picture_folder);
    system(command);
}
```
**Penjelasan Kode :**
- Pada fungsi ``movePictures()``, dilakukan pembuatan direktori baru serta pemindahan file sesuai dengan tempat tinggal binatangnya dengan menggunakan fungsi ``system`` yang didalamnya terdapat script untuk melakukan hal tersebut. Pada script, pertama kita membuat direktori-direktorinya terlebih dahulu, kemudian pada direktori dimana folder binatangnya tersimpan akan dilakukan pengecekan untuk memindahkan filenya ke direktori baru. Nama akhir dari file binatangnya akan mendetermine direktori mana file tersebut akan disimpan, maka dari itu akan diextract dan disimpan pada ``last_name``. Kemudian dilakukan ``mv`` untuk setiap file yang sesuai dengan direktorinya.

### Soal D
**Solusi :**

```c
// zip folder hewan air
waitpid(pid, &status, 0);
pid = fork();

if (pid < 0) {
    perror("fork");
}

if (pid == 0) {
    execlp("zip", "zip", "HewanAir.zip", "HewanAir", NULL);
}

else {
    // zip folder hewan amphibi
    waitpid(pid, &status, 0);
    pid = fork();

    if (pid < 0) {
        perror("fork");
    }

    if (pid == 0) {
        execlp("zip", "zip", "HewanAmphibi.zip", "HewanAmphibi", NULL);
    }

    else {
        // zip folder hewan darat
        waitpid(pid, &status, 0);
        pid = fork();

        if (pid < 0) {
            perror("fork");
        }

        if (pid == 0) {
            execlp("zip", "zip", "HewanDarat.zip", "HewanDarat", NULL);
        }

        else {
            ...
```
**Penjelasan Kode :**
- Selanjutnya, program melakukan zip untuk setiap direktori baru yang telah dibuat pada poin C. Untuk melakukannya, dibuat beberapa child process baru yang masing-masing melakukan zip untuk setiap direktori menggunakan ``execlp("zip", "zip", "nama_file")``. Ketiga child process akan saling menunggu child process sebelumnya, yang dimulai dari pengezipan folder hewan air, kemudian hewan amphibi, dan terakhir hewan darat.

Terakhir, kami melakukan deletion pada file-file yang tidak diperlukan dengan membuat child process baru yang menunggu child process sebelumnya selesai. Hal ini dilakukan dengan menggunakan fungsi ``deleteFolders()``. Berikut implementasinya.
```c
void deleteFolders() {
    char command[1000];
    
    // delete the HewanAir, HewanAmphibi, and HewanDarat folders
    sprintf(command, "rm -r HewanAir HewanAmphibi HewanDarat");
    system(command);

    // delete the binatang.zip file
    sprintf(command, "rm binatang.zip");
    system(command);
}
.
.
.
main()

// delete unnecessary files/folders
waitpid(pid, &status, 0);
pid = fork();

if (pid < 0) {
    perror("fork");
}

if (pid == 0) {
    deleteFolders();
}
```
**Jawaban:**

```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <dirent.h>

// Rayhan Arvianta Bayuputra 5025211217

void randomSelect() {
    char *picture_folder = "/home/arvianta/sisop/P2/NO1";
    char command[1000];
    sprintf(command, "/bin/bash -c '"
                    "picture_folder=\"%s\" && "
                    "picture_files=($picture_folder/*.jpg) && "
                    "if [ ${#picture_files[@]} -eq 0 ]; then "
                    "echo \"No .jpg files found in the folder\" && exit 1; "
                    "fi && "
                    "random_index=$((RANDOM %% ${#picture_files[@]})) && "
                    "random_picture=\"$(basename ${picture_files[$random_index]} .jpg)\" && "
                    "echo \"Pada shift kali ini, Grape-Kun menjaga $random_picture\"'"
                    , picture_folder);
    system(command);
}

void movePictures() {
    char *picture_folder = "/home/arvianta/sisop/P2/NO1";
    char command[2000];
    sprintf(command, "/bin/bash -c '"
            "# get the picture folder from the first argument\n"
            "picture_folder=\"%s\"\n"
            "\n"
            "# create directories for each type of animal\n"
            "mkdir -p \"$picture_folder/HewanDarat\"\n"
            "mkdir -p \"$picture_folder/HewanAmphibi\"\n"
            "mkdir -p \"$picture_folder/HewanAir\"\n"
            "\n"
            "# loop through picture files in folder\n"
            "for file in \"$picture_folder\"/*; do\n"
            "  # get the filename without path and extension\n"
            "  filename=$(basename -- \"$file\")\n"
            "  extension=\"${filename##*.}\"\n"
            "  filename=\"${filename%%.*}\"\n"
            "  \n"
            "  # get the last name of the animal\n"
            "  last_name=\"${filename##*_}\"\n"
            "  \n"
            "  # move the file to the appropriate directory based on last name\n"
            "  if [[ \"$last_name\" == \"darat\" ]]; then\n"
            "    mv \"$file\" \"$picture_folder/HewanDarat/$filename.$extension\"\n"
            "  elif [[ \"$last_name\" == \"amphibi\" ]]; then\n"
            "    mv \"$file\" \"$picture_folder/HewanAmphibi/$filename.$extension\"\n"
            "  elif [[ \"$last_name\" == \"air\" ]]; then\n"
            "    mv \"$file\" \"$picture_folder/HewanAir/$filename.$extension\"\n"
            "  fi\n"
            "done'"
            , picture_folder);
    system(command);
}

void deleteFolders() {
    char command[1000];
    
    // delete the HewanAir, HewanAmphibi, and HewanDarat folders
    sprintf(command, "rm -r HewanAir HewanAmphibi HewanDarat");
    system(command);

    // delete the binatang.zip file
    sprintf(command, "rm binatang.zip");
    system(command);
}

int main()
{    
    pid_t pid;
    int status;
    pid = fork();

    if (pid < 0) {
        perror("fork");
    }
    if(pid == 0) {
        execlp("wget", "wget", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL);
        exit(0);
    } else {
        // unzip
        waitpid(pid, &status, 0);
        pid = fork();
        
        if (pid < 0) {
            perror("fork");
        }
        if (pid == 0) {
            execlp("unzip", "unzip", "binatang.zip", NULL);
        } else {
            // random select
            waitpid(pid, &status, 0);
            pid = fork();

            if (pid < 0) {
                perror("fork");
            }

            if (pid == 0) {
                randomSelect();
            }
            else {
                // move pictures
                waitpid(pid, &status, 0);
                pid = fork();

                if (pid < 0) {
                    perror("fork");
                }

                if (pid == 0) {
                    movePictures();
                }

                else {
                    // zip folder hewan air
                    waitpid(pid, &status, 0);
                    pid = fork();

                    if (pid < 0) {
                        perror("fork");
                    }

                    if (pid == 0) {
                        execlp("zip", "zip", "HewanAir.zip", "HewanAir", NULL);
                    }

                    else {
                        // zip folder hewan amphibi
                        waitpid(pid, &status, 0);
                        pid = fork();

                        if (pid < 0) {
                            perror("fork");
                        }

                        if (pid == 0) {
                            execlp("zip", "zip", "HewanAmphibi.zip", "HewanAmphibi", NULL);
                        }

                        else {
                            // zip folder hewan darat
                            waitpid(pid, &status, 0);
                            pid = fork();

                            if (pid < 0) {
                                perror("fork");
                            }

                            if (pid == 0) {
                                execlp("zip", "zip", "HewanDarat.zip", "HewanDarat", NULL);
                            }

                            else {
                                // delete unnecessary files/folders
                                waitpid(pid, &status, 0);
                                pid = fork();

                                if (pid < 0) {
                                    perror("fork");
                                }

                                if (pid == 0) {
                                    deleteFolders();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return 0;
}

```
**Screenshot Output Program Nomor 1 :**
| <p align="center"> Screenshot </p> | <p align="center"> Deskripsi </p> |
| ---------------------------------- | ----------------------------------- |
|<img src="/uploads/e3cd13e66976a134ed90dddf36c3a9f3/no1.png" width = "350"/> | <p align="center">Program berjalan dengan baik, mulai dari proses download, unzip, randomSelect, mkdir, move, hingga zip dan delete unnecessary files.</p> |

## Nomor 2
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi!

a. Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

b. Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

c. Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

d. Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

e. Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

Catatan:
- Tidak boleh menggunakan system()
- Proses berjalan secara daemon
- Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)


**Solusi:**

```c
void killer(bool mode, pid_t sid){
```
-	**Pertama**, membuat fungsi dengan dua parameter, ``mode`` dan ``sid`` yang bertujuan untuk menghentikan program.

```c
FILE *filekill = fopen("killer.c", "w+");
```
-	**Kedua**, deklarasi variabel ``filekill`` sebagai pointer struktur FILE dan membuka file _killer.c_ dengan ``w++`` yang mana file akan dibuka.

```c
fprintf(filekill, "#include <stdio.h>\n");
fprintf(filekill, "#include <stdlib.h>\n");
fprintf(filekill, "#include <unistd.h>\n");
fprintf(filekill, "#include <wait.h>\n");
```
-	``#include <stdio.h>`` adalah fungsi input/output standar.
-	``#include <stdlib.h>`` adalah fungsi-fungsi bantu seperti malloc(), free(), exit(), dll.
-	``#include <unistd.h>`` adalah fungsi-fungsi untuk mengakses seperti fork(), exec(), dll.
-	``#include <wait.h>`` adalah fungsi-fungsi untuk mengontrol proses seperti wait(), waitpid(), dll.


```c
fprintf(filekill, "int main() {\n");
```
-	**Keempat**, buat kode untuk mematikan jalannya program.

```c
fprintf(filekill, "pid_t child_id = fork();\n");
fprintf(filekill, "if (child_id == 0) {\n");
```
-	``pid_t child id = fork()`` mendeklarasikan _pid_t_ dengan nama _child_id_ dan menetapkan nilai variabel tersebut dengan _fork()_ untuk membuat proses baru yang disalin dari _parent_.
-	``if (child_id==0)`` memeriksa jika nilai _child_id_ adalah 0, menandakan program sedang berjalan dalam proses _child_.

```c
if (mode == true) {
fprintf(filekill, "execl(\"/usr/bin/pkill\", \"pkill\", \"-9\", \"-s\", \"%d\", NULL);\n", sid);
} else if (mode == false) {
	fprintf(filekill, "execl(\"/bin/kill\", \"kill\", \"-9\", \"%d\", NULL);\n", getpid());
}
```
-	``if (mode == true)`` memeriksa jika mode bernilai true, maka program akan dieksekusi.
-	``fprintf(filekill, "execl("/usr/bin/pkill", "pkill", "-9", "-s", "%d", NULL);\n", sid);`` menjalankan program _pkill_ dengan menggunakan _execl()_. _pkill_ digunakan untuk mengirim _SIGKILL (-9)_ ke proses yang dijalankan. Nilai _sid_ akan dimasukkan ke dalam string sebagai argumen ke-5. _NULL_ menandakan bahwa tidak ada argumen tambahan yang diperlukan.
-	``else if (mode == false)`` memeriksa jika mode bernilai false, maka program akan dieksekusi.
-	``fprintf(filekill, "execl("/bin/kill", "kill", "-9", "%d", NULL);\n", getpid());``menuliskan perintah untuk menjalankan _kill_ menggunakan _execl()_. Program "kill" digunakan untuk mengirim _SIGKILL (-9)_ ke proses yang disimpan dalam _getpid()_. Nilai _getpid()_ akan dimasukkan sebagai argumen ke-4. NULL menandakan bahwa tidak ada argumen tambahan yang diperlukan.




```c
fprintf(filekill, "}\n");
fprintf(filekill, "while(wait(NULL) > 0);\n");
fprintf(filekill, "execl(\"/usr/bin/rm\", \"rm\", \"killer\", NULL);\n");
fprintf(filekill, "return 0; }\n");
```
-	``fprint(filekill, “}\n”)`` menutup blok program baris sebelumnya.
-	``fprintf(filekill, "while(wait(NULL) > 0);\n")`` menulis perintah _while()_ yang menggunakan _wait()_ untuk menunggu hingga proses _child_ selesai dijalankan. _wait()_ akan me-return nilai positif jika masih ada proses _child_ yang berjalan dan nilai 0 jika tidak ada yang berjalan.
-	``fprintf(filekill, "execl(\"/usr/bin/rm\", \"rm\", \"killer\", NULL);\n")`` menghapus file _killer_ setelah proses _child_ selesai dijalankan. _execl()_ digunakan untuk menjalankan _rm_ dengan dua argument, ‘rm" dan "killer". NULL menandakan bahwa tidak ada argumen tambahan yang diperlukan.
-	`` fprintf(filekill, "return 0; }\n");`` menutup main() dan me-return nilai 0 sebagai tanda program sukses.

```c
fclose(filekill);
```
-	``fclose(filekill)`` menutup file _killer.c_ yang telah dibuka.

```c
int flag = 0;
```
-	`` int flag = 0`` mendeklarasikan variabel _flag_ dengan nilai 0.

```c
if(child_id == 0){
execl("/usr/bin/gcc", "gcc", "killer.c", "-o", "killer", NULL);
}
waitpid(child_id, &flag, 0);
```
-	**Lalu**, eksekusi _gcc killer.c -o killer_ pada proses _child_ jika proses _parent_ bernilai 0.

```c
child_id = fork();
if (child_id == 0) {
    execl("/usr/bin/rm", "rm", "killer.c", NULL);
}
```
-	`` child_id = fork()`` membuat sebuah proses _child_ baru dan me-return nilai 0.
-	``if(child_id == 0)`` mengeksekusi _rm killer.c_ jika nilai _child_id_ adalah 0.


```c
int main(int argc, char* argv[]){
```
-	Inisialisasi _main function_.

```c
if(argc == 1){
	printf("Use argumet -a or -b\n");
	exit(0);
}
if(strcmp("-a", argv[1]) == 0) {
printf("MODE_A\n");
mode = true;
} 
	else if (strcmp("-b", argv[1]) == 0) {
		printf("MODE_B\n");
        mode = false;
    } 
	else {
        printf("Input the right mode (-a or -b)\n");
        exit(1);
    }
```
-	Jika nilai ``argc`` adalah 1, maka program akan mencetak “Use argument -a or -b.”
-	Lalu, program akan memerika jika argument yang dimasukkan adalah -a, maka program menjalankan dalam mode A. Jika argumen yang dimasukkan adalah -b, maka program akan menjalankan mode B.
-	Jika argumen yanag dimasukkan bukan -a atau -b, maka program akan menampilkan pesan "Input the right mode (-a or -b)" dan keluar dari program menggunakan fungsi exit(1) yang menandakan error.



```c
pid_t pid, sid;
pid = fork();
```
-	``pid_t pid, sid`` merupakan variabel untuk meyimpak _Parent ID_ dan ``pid = fork()`` bertujuan untuk menyimpan ``PID`` dari _child_.

```c
if (pid < 0) {
    exit(EXIT_FAILURE);
}
if (pid > 0) {
    exit(EXIT_SUCCESS);
}
```
-	**Selanjutnya**, jika nilai pid kurang dari 0, menandakan program gagal dijalankan. Jika nilai pid lebih dari 0, menandakan program berhasil dijalankan.

```c
sid = setsid();
if (sid < 0) {
exit(EXIT_FAILURE);
}
```
-	``setsid()`` membuat sesi baru untuk proses selanjutnya. Jika nilai yang direturn kurang dari 0, menandakan sesi baru gagal dibuat.

```c
killer(mode, sid);
close(STDIN_FILENO); 
close(STDOUT_FILENO); 
close(STDERR_FILENO); 
```
-	Fungsi bawaan untuk memerika mode program.

```c
while(1){
```
-	Dilakukan iterasi ``while(1)`` yang dijalankan setiap 1 detik dengan rincian di bawah ini.

```c
char foldernm[20];
time_t ctime = time(NULL);
struct tm* ttime = localtime(&ctime);
strftime(foldernm, sizeof(foldernm), "%Y-%m-%d_%H:%M:%S", ttime);
```
-	``char foldernm[20]`` mendeklarasikan sebuah array dengan ukuran 20.
-	``time_t ctime = time(NULL)`` sebagai penampung waktu saat ini.
-	``struct tm* ttime = localtime(&ctime)`` sebagai pointer yang menampung informasi waktu lokal.
-	`` strftime(foldernm, sizeof(foldernm), "%Y-%m-%d_%H:%M:%S", ttime)`` membuat string dengan format waktu “YYYY-MM-DD_HH:MM:SS”.

```c
pid_t pid = fork();
```
-	``fork()`` membuat proses _child_ yang akan disimpan di pid.

```c
if(pid == 0){
	mkdir(foldernm, 0777);
	    	
	pid_t down;
	char imagenm[25], 
            loc[50], 
            link[50];

for(int i = 0; i < 15; i++){
    down = fork();
        if(down == 0){
            time_t ctime2 = time(NULL);
            struct tm* ttime2 = localtime(&ctime2);
		    strftime(imagenm, sizeof(imagenm),"%Y-%m-%d_%H:%M:%S.jpg", ttime2);
		            		sprintf(loc, "%s/%s", foldernm, imagenm);
		            		sprintf(link, "https://picsum.photos/%ld", (ctime2 % 1000) + 50);		            
		            		execl("/usr/bin/wget", "wget", "-q", "-O", loc, link, NULL);
		        	}
		        sleep(5);
}
while(wait(NULL) > 0);

		pid_t zip = fork();
		if(zip == 0) {
                		execl("/usr/bin/zip", "zip", "-r", foldernm, foldernm, NULL);
            		}
            		int statuszip = 0;
		waitpid(zip, &statuszip, 0);
            		execl("/usr/bin/rm", "rm", "-r", foldernm, NULL);
	}
	sleep(30);
```
- Kondisi if akan memerika jika pid kurang dari 0, maka proses saat ini adalah proses _child_ yang dibuat dengan ``fork()``.
- Lalu, program akan membaur direktori baru menggunakan ``mkdir()``. Nama direktori baru disimpan dalam variabel _foldernm_.
- Program menginisialisasi ``pid_t`` dan tiga array karakter bernama ``imagenm, loc,`` dan ``link``.
- Sebuah loop for kemudian digunakan untuk membuat 15 proses _child_ menggunakan ``fork()``. Setiap proses _child_ nama file menggunakan ``strftime()``, membuat jalur file lengkap dengan menggabungkan nama folder dan nama file.
- URL gambar dibuat menggunakan ``sprintf()`` dengan variabel ``ctime2`` sebagai generator angka acak. ``sleep()`` dibuat untuk menjeda iterasi.
- Proses _parent_ menunggu semua proses _child_ selesai menggunakan fungsi ``wait()``.
- Setelah semua proses _child_ selesai, _parent_ membuat proses _child_ baru menggunakan fork() untuk mengompresi gambar yang didownload ke dalam file ZIP. Proses _child_ kemudian menghapus direktori asli menggunakan ``rm``. 
Program menunggu proses _child_ selesai menggunakan ``waitpid()``.



**Jawaban:**

```c
//Danno Denis Dhaifullah 5025211027 && Rayhan Arvianta Bayuputra 5025211217
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>
#include<signal.h>
#include<unistd.h>
#include<syslog.h>
#include<fcntl.h>
#include<errno.h>
#include<time.h>
#include<wait.h>
#include<sys/prctl.h>
#include<sys/types.h>
#include<sys/stat.h>

//membuat killer.c sesuai dengan ketentuan mode a dan b
void killer(bool mode, pid_t sid){
	
	//akses write ke file
    FILE *filekill = fopen("killer.c", "w+");
    
    //header killer.c
    fprintf(filekill, "#include <stdio.h>\n");
    fprintf(filekill, "#include <stdlib.h>\n");
    fprintf(filekill, "#include <unistd.h>\n");
    fprintf(filekill, "#include <wait.h>\n");
    
    //main func
    fprintf(filekill, "int main() {\n");

    //fork dahulu untuk melihat proses, apabila 0 cek argumen mode
    fprintf(filekill, "pid_t child_id = fork();\n");
    fprintf(filekill, "if (child_id == 0) {\n");
    
    //MODE_A
    char argmode[100];
    if (mode == true) {
        fprintf(filekill, "execl(\"/usr/bin/pkill\", \"pkill\", \"-9\", \"-s\", \"%d\", NULL);\n", sid);
    }
    
    //MODE_B
    else if (mode == false) {
        fprintf(filekill, "execl(\"/bin/kill\", \"kill\", \"-9\", \"%d\", NULL);\n", getpid());        
    }
    
    fprintf(filekill, "}\n");
    fprintf(filekill, "while(wait(NULL) > 0);\n");
    fprintf(filekill, "execl(\"/usr/bin/rm\", \"rm\", \"killer\", NULL);\n");
    fprintf(filekill, "return 0; }\n");

    fclose(filekill);
	
	int flag = 0;

    // compile killer.c
    pid_t child_id = fork();
    if(child_id == 0){
        execl("/usr/bin/gcc", "gcc", "killer.c", "-o", "killer", NULL);
    }
	waitpid(child_id, &flag, 0);

    // hapus killer.c
    child_id = fork();
    if (child_id == 0) {
    	execl("/usr/bin/rm", "rm", "killer.c", NULL);
    }
    
    //setelah dicompile akan membentuk file killer, lalu file killer.c dihapus.
}

int main(int argc, char* argv[]){
    // Check argument
	bool mode;
	if(argc == 1){
		printf("Use argumet -a or -b\n");
		exit(0);
	}
    if(strcmp("-a", argv[1]) == 0) {
    	printf("MODE_A\n");
        mode = true;
    } 
	else if (strcmp("-b", argv[1]) == 0) {
		printf("MODE_B\n");
        mode = false;
    } 
	else {
        printf("Inputkan the right mode (-a or -b)\n");
        exit(1);
    }
    
	pid_t pid, sid; // Variabel untuk menyimpan PID
	pid = fork();   // Menyimpan PID dari child

	/* Keluar saat fork gagal (nilai variabel pid < 0) */
	if (pid < 0) {
		exit(EXIT_FAILURE);
  	}

	if (pid > 0) {
	   exit(EXIT_SUCCESS);
	}

	umask(0);

  	sid = setsid();
  	if (sid < 0) {
    	exit(EXIT_FAILURE);
  	}
  	
	//check program mode
	killer(mode, sid);
  	close(STDIN_FILENO); //input
  	close(STDOUT_FILENO); //output
  	close(STDERR_FILENO); //error

    while(1){
        char foldernm[20];
		time_t ctime = time(NULL);
		struct tm* ttime = localtime(&ctime);
	    strftime(foldernm, sizeof(foldernm), "%Y-%m-%d_%H:%M:%S", ttime);
       
        pid_t pid = fork();
	    if(pid == 0){
	    	//membuat folder
	    	mkdir(foldernm, 0777);
	    	
	    	//download gambar
	    	pid_t down;
		    char imagenm[25], 
				 loc[50], 
				 link[50];
		   
		   	//loop 15x, untuk mendapat 15 gambar setiap foldernya. 
		    for(int i = 0; i < 15; i++){
		        down = fork();
		        if(down == 0){
		            time_t ctime2 = time(NULL);
		            struct tm* ttime2 = localtime(&ctime2);
		            strftime(imagenm, sizeof(imagenm), "%Y-%m-%d_%H:%M:%S.jpg", ttime2);
		            sprintf(loc, "%s/%s", foldernm, imagenm);
		            sprintf(link, "https://picsum.photos/%ld", (ctime2 % 1000) + 50);		            
		            execl("/usr/bin/wget", "wget", "-q", "-O", loc, link, NULL);
		        }
		        sleep(5);
		    }
		    while(wait(NULL) > 0);
		    
		    //zip folder
		    pid_t zip = fork();
		    if(zip == 0) {
                execl("/usr/bin/zip", "zip", "-r", foldernm, foldernm, NULL);
            }
            int statuszip = 0;
		    waitpid(zip, &statuszip, 0);
            execl("/usr/bin/rm", "rm", "-r", foldernm, NULL);
		}
		
        sleep(30);
    }
}
```
**Screenshot Output Program Nomor 2 :**
| <p align="center"> Screenshot </p> | <p align="center"> Deskripsi </p> |
| ---------------------------------- | --------------------------------- |
|<img src="/uploads/38eb12eb3ef1da6d68e1e4f87dca9c7c/no2a.png" width = "350"/> | <p align="center"> Program berjalan dengan Mode A</p> |
|<img src="/uploads/af2a1804eb47959128a2f2d577d3563e/no2b.png" width = "350"/> | <p align="center"> Pemanggilan killer dengan Mode A, proses download langsung terhenti.</p> |
|<img src="/uploads/56f1e5e527a27438972a0a72320db716/no2c.png" width = "350"/> | <p align="center"> Program berjalan dengan Mode B</p> |
|<img src="/uploads/80f66671180fcd750683139177be997e/no2d.png" width = "350"/> | <p align="center"> Pemanggilan killer dengan Mode B, proses download masih berlanjut hingga selesai.</p> |


## Nomor 3
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut **hanya dengan 1 Program C** bernama **“filter.c”.**

a. Pertama-tama, Program filter.c akan **mengunduh** file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan **extract** “players.zip”. Lalu **hapus** file zip tersebut agar tidak memenuhi komputer Ten Hag.

b. Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang **bukan** dari Manchester United yang ada di directory.

c. Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam **waktu bersamaan dengan 4 proses yang berbeda.** Untuk kategori folder akan menjadi 4 yaitu **Kiper, Bek, Gelandang, dan Penyerang.**

d. Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan **Kesebelasan Terbaik** untuk menjadi senjata utama MU berdasarkan **rating** terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi **Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt** dan akan ditaruh di /home/[users]/

Catatan:

- Format nama file yang akan diunduh dalam zip dan isi txt formasi berupa [nama]_[tim]_[posisi]_[rating].png
- Tidak boleh menggunakan system()
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec() dan fork().
- Directory “.” dan “..” tidak termasuk yang akan dihapus.
- Untuk poin d **DIWAJIBKAN** membuat fungsi bernama **buatTim(int, int, int)**, dengan input 3 value integer dengan urutan bek, gelandang, dan striker.

### Soal A 
**Solusi :**
```c
pid = fork();

    if (pid < 0){
        perror("fork");
    }
    if (pid == 0){
        execlp("wget", "wget", "-O", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL);
        exit(0);
    } else {
        // unzip
        waitpid(pid, &status, 0);
        pid = fork();

        if (pid < 0){
            perror("fork");
        }
        if (pid == 0){
            execlp("unzip", "unzip", "players.zip", NULL);
        } else {
            // delete players.zip
            waitpid(pid, &status, 0);
            pid = fork();
            if (pid < 0){
                perror("fork");
            }
            if (pid == 0){
                execlp("rm", "rm", "players.zip", NULL);
            }
            else {
                ...
```
**Penjelasan Kode :**
- Dilakukan pembuatan child baru dengan ``fork()`` yang mana child pertama akan melakukan download file yang disimpan sebagai ``players.zip`` menggunakan ``execlp()`` yang didalamnya diberi perintah ``wget``. Kemudian, child kedua akan dispawn setelah child pertama selesai melakukan download. Child kedua akan melakukan unzip dengan menggunakan ``execlp`` yang berisi perintah ``unzip``. Kemudian, dilakukan spawning child process kembali untuk melakukan deletion pada file ``players.zip`` menggunakan ``execlp`` yang berisi script ``rm``. Child process ini juga menunggu child process sebelumnya selesai. 

### Soal B 
**Solusi :**
```c
// delete non MU
waitpid(pid, &status, 0);
pid = fork();

if (pid < 0){
    perror("fork");
}
if (pid == 0){
    chdir("players");
    execlp("find", "find", ".", "-type", "f", "-not", "-name", "*ManUtd*", "-delete", NULL);
} else {
    ...
```
**Penjelasan Kode :**
- Dilakukan spawning child process baru, namun tetap menunggu child process sebelumnya selesai. Pada child process ini, dilakukan deletion file players yang bukan pemain dari Manchester United. Hal ini dilakukan dengan menggunakan fungsi ``execlp`` yang didalamnya berisikan script ``find . -type f -not -name *ManUtd* -delete``. Script tersebut mencari file pada direktori saat ini (``.``), mencari file bertipe standard (``-type f``), yang mana nama filenya tidak mengandung string ManUtd (``-not -name *ManUtd*``), dan akhirnya di delete.

### Soal C 
**Solusi :**
```c
// group players dir
waitpid(pid, &status, 0);
pid = fork();

if (pid < 0){
    perror("fork");
}
if (pid == 0){
    chdir("players");
    execlp("mkdir", "mkdir", "Bek", "Gelandang", "Kiper", "Penyerang", NULL);
} else {
    // move players
    waitpid(pid, &status, 0);
    int i;
    pid_t mpid[4];

    // create four child processes
    for (i = 0; i < 4; i++) {
        mpid[i] = fork();
        if (mpid[i] < 0) {
            // fork failed
            perror("fork");
        }
        
        if (mpid[i] == 0) {
            // child process
            switch(i) {
                case 0:
                    chdir("players");
                    execlp("find", "find", ".", "-type", "f", "-name", "*Kiper_*", "-exec", "mv", "{}", "Kiper/", ";", NULL);
                    break;
                case 1:
                    chdir("players");
                    execlp("find", "find", ".", "-type", "f", "-name", "*Bek_*", "-exec", "mv", "{}", "Bek/", ";", NULL);
                    break;
                case 2:
                    chdir("players");   
                    execlp("find", "find", ".", "-type", "f", "-name", "*Gelandang_*", "-exec", "mv", "{}", "Gelandang/", ";", NULL);
                    break;
                case 3:
                    chdir("players");
                    execlp("find", "find", ".", "-type", "f", "-name", "*Penyerang_*", "-exec", "mv", "{}", "Penyerang/", ";", NULL);
                    break;
            }
        } 
    }
    ...
```
**Penjelasan Kode :**
- Pertama, dibuat child process baru yang melakukan pembuatan direktori baru untuk tiap-tiap posisi pemain dengan menggunakan ``execlp`` yang berisi ``mkdir``. Setelah itu, dibuat 4 child process baru yang akan berjalan bersamaan menggunakan looping, yang mana setiap child processnya akan melakukan processnya masing-masing yang dinyatakan menggunakan switch-case sesuai iterasinya. Pada setiap casenya, dilakukan chdir ke direktori ``players``. Kemudian, pemindahan file-file playersnya dilakukan menggunakan ``execlp`` yang berisi perintah ``find`` serta ``mv`` sesuai dengan nama dari filenya.

### Soal D 
**Solusi :**
```c
void buatTim(int bek, int gelandang, int penyerang)
{
    Player pemain_gelandang[100], pemain_penyerang[100], kiper[1], pemain_bek[100];
    int jumlah_gelandang = 0, jumlah_penyerang = 0, jumlah_kiper = 0, jumlah_bek = 0;

    int total_players = bek + gelandang + penyerang + 1;
    if (total_players != 11) {
        printf("There must be exactly 11 players in the team! With your formation, there are %d players.\n", total_players);
        return;
    } 

    getPlayerRating("Bek", pemain_bek, &jumlah_bek);
    if (jumlah_bek < bek) {
        printf("Insufficient bek players in the team.\n");
        return;
    }

    getPlayerRating("Gelandang", pemain_gelandang, &jumlah_gelandang);
    if (jumlah_gelandang < gelandang) {
        printf("Insufficient gelandang players in the team.\n");
        return;
    }

    getPlayerRating("Penyerang", pemain_penyerang, &jumlah_penyerang);
    if (jumlah_penyerang < penyerang) {
        printf("Insufficient penyerang players in the team.\n");
        return;
    }

    getPlayerRating("Kiper", kiper, &jumlah_kiper);

    qsort(pemain_gelandang, jumlah_gelandang, sizeof(Player), compareRate);
    qsort(pemain_penyerang, jumlah_penyerang, sizeof(Player), compareRate);
    qsort(pemain_bek, jumlah_bek, sizeof(Player), compareRate);

    char filepath[300];
    snprintf(filepath, 300, "/home/arvianta/Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
    FILE *file = fopen(filepath, "w");

    if (file)
    {
        int i;
        fprintf(file, "=====Kiper=====\n%s\n\n", kiper[0].filename);

        fprintf(file, "=====Bek=====\n");
        for (i = 0; i < bek && i < jumlah_bek; i++)
        {
            fprintf(file, "%d. %s\n", i + 1, pemain_bek[i].filename);
        }

        fprintf(file, "\n=====Gelandang=====\n");
        for (i = 0; i < gelandang && i < jumlah_gelandang; i++)
        {
            fprintf(file, "%d. %s\n", i + 1, pemain_gelandang[i].filename);
        }

        fprintf(file, "\n=====Penyerang=====\n");
        for (i = 0; i < penyerang && i < jumlah_penyerang; i++)
        {
            fprintf(file, "%d. %s\n", i + 1, pemain_penyerang[i].filename);
        }

        fclose(file);
        printf("Lineup saved\n");
    }
    else
    {
        printf("Error\n");
        exit(EXIT_FAILURE);
    }
}
```
**Penjelasan Kode :**
- Pada fungsi, terdapat 3 parameter yang merupakan formasi yang akan dibentuk. Pertama-tama, kita inisiasi array of Players yang mana struct players adalah sebagai berikut.
```c
typedef struct
{
    int rating;
    char filename[300];
} Player;
```
- Kemudian, inisiasi juga jumlah dari setiap posisi pemain yang akan digunakan pada sorting nantinya. Setelah itu, dilakukan pengecekan apabila input dari formasi sesuai dengan peraturan formasi sepakbola (harus 11 orang). Apabila lolos pengecekan, masuk ke fungsi ``getPlayerRating()``.
```c
void getPlayerRating(const char *position, Player *players, int *count)
{
    struct dirent *entry;
    DIR *dir;
    char directory[100];

    snprintf(directory, 100, "./players/%s", position);
    dir = opendir(directory);

    if (dir)
    {
        for (entry = readdir(dir); entry; entry = readdir(dir))
        {
            if (strstr(entry->d_name, ".png") && entry->d_type == DT_REG)
            {
                int rating;
                sscanf(entry->d_name, "%*[^_]_%*[^_]_%*[^_]_%d.png", &rating);
                strcpy(players[*count].filename, entry->d_name);
                players[*count].rating = rating;
                (*count)++;
            }
        }
        closedir(dir);
    }
    else
    {
        exit(EXIT_FAILURE);
    }
}
```
- Pada fungsi tersebut, kita akan mengembalikan rating-rating dari pemainnya yang akan dimasukkan ke dalam array of Players yang kita inisiasi sebelumnya. Selain itu, didapatkan juga berapa jumlah pemain sesuai dengan posisinya pada Manchester United. 
- Setelah kita mendapatkan data rating pemainnya serta jumlahnya, akan dilakukan pengecekan kembali apabila pemain yang tersedia cukup untuk membuat formasi yang akan dibentuk. Hal ini dilakukan untuk semua posisi pemain.
- Kemudian, dengan menggunakan fungsi ``qsort`` akan dilakukan sorting pemain berdasarkan ratingnya pada tiap-tiap posisi yang juga men-utilize fungsi ``compareRate()``.
```c
int compareRate(const void *a, const void *b)
{
    const Player *player_a = (const Player *)a;
    const Player *player_b = (const Player *)b;
    return player_b->rating - player_a->rating;
}
```
- Pada fungsi tersebut, akan dikembalikan perbedaan antara rating dari dua pemain (``player_b->rating - player_a->rating``) yang akan digunakan pada qsort untuk melakukan sorting secara descending.
- Setelah mendapatkan array yang sudah terurut, hasilnya akan diprint ke file .txt yang diletakkan pada direktori ``home/[users]``. 


**Jawaban:**

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>

// Rayhan Arvianta Bayuputra 5025211217

typedef struct
{
    int rating;
    char filename[300];
} Player;

int compareRate(const void *a, const void *b)
{
    const Player *player_a = (const Player *)a;
    const Player *player_b = (const Player *)b;
    return player_b->rating - player_a->rating;
}

void getPlayerRating(const char *position, Player *players, int *count)
{
    struct dirent *entry;
    DIR *dir;
    char directory[100];

    snprintf(directory, 100, "./players/%s", position);
    dir = opendir(directory);

    if (dir)
    {
        for (entry = readdir(dir); entry; entry = readdir(dir))
        {
            if (strstr(entry->d_name, ".png") && entry->d_type == DT_REG)
            {
                int rating;
                sscanf(entry->d_name, "%*[^_]_%*[^_]_%*[^_]_%d.png", &rating);
                strcpy(players[*count].filename, entry->d_name);
                players[*count].rating = rating;
                (*count)++;
            }
        }
        closedir(dir);
    }
    else
    {
        exit(EXIT_FAILURE);
    }
}

void buatTim(int bek, int gelandang, int penyerang)
{
    Player pemain_gelandang[100], pemain_penyerang[100], kiper[1], pemain_bek[100];
    int jumlah_gelandang = 0, jumlah_penyerang = 0, jumlah_kiper = 0, jumlah_bek = 0;

    int total_players = bek + gelandang + penyerang + 1;
    if (total_players != 11) {
        printf("There must be exactly 11 players in the team! With your formation, there are %d players.\n", total_players);
        return;
    } 

    getPlayerRating("Bek", pemain_bek, &jumlah_bek);
    if (jumlah_bek < bek) {
        printf("Insufficient bek players in the team.\n");
        return;
    }

    getPlayerRating("Gelandang", pemain_gelandang, &jumlah_gelandang);
    if (jumlah_gelandang < gelandang) {
        printf("Insufficient gelandang players in the team.\n");
        return;
    }

    getPlayerRating("Penyerang", pemain_penyerang, &jumlah_penyerang);
    if (jumlah_penyerang < penyerang) {
        printf("Insufficient penyerang players in the team.\n");
        return;
    }

    getPlayerRating("Kiper", kiper, &jumlah_kiper);

    qsort(pemain_gelandang, jumlah_gelandang, sizeof(Player), compareRate);
    qsort(pemain_penyerang, jumlah_penyerang, sizeof(Player), compareRate);
    qsort(pemain_bek, jumlah_bek, sizeof(Player), compareRate);

    char filepath[300];
    snprintf(filepath, 300, "/home/arvianta/Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
    FILE *file = fopen(filepath, "w");

    if (file)
    {
        int i;
        fprintf(file, "=====Kiper=====\n%s\n\n", kiper[0].filename);

        fprintf(file, "=====Bek=====\n");
        for (i = 0; i < bek && i < jumlah_bek; i++)
        {
            fprintf(file, "%d. %s\n", i + 1, pemain_bek[i].filename);
        }

        fprintf(file, "\n=====Gelandang=====\n");
        for (i = 0; i < gelandang && i < jumlah_gelandang; i++)
        {
            fprintf(file, "%d. %s\n", i + 1, pemain_gelandang[i].filename);
        }

        fprintf(file, "\n=====Penyerang=====\n");
        for (i = 0; i < penyerang && i < jumlah_penyerang; i++)
        {
            fprintf(file, "%d. %s\n", i + 1, pemain_penyerang[i].filename);
        }

        fclose(file);
        printf("Lineup saved\n");
    }
    else
    {
        printf("Error\n");
        exit(EXIT_FAILURE);
    }
}

int main()
{
    pid_t pid;
    int status;
    pid = fork();

    if (pid < 0)
    {
        perror("fork");
    }
    if (pid == 0)
    {
        execlp("wget", "wget", "-O", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL);
        exit(0);
    }
    else
    {
        // unzip
        waitpid(pid, &status, 0);
        pid = fork();

        if (pid < 0)
        {
            perror("fork");
        }
        if (pid == 0)
        {
            execlp("unzip", "unzip", "players.zip", NULL);
        }
        else
        {
            // delete players.zip
            waitpid(pid, &status, 0);
            pid = fork();
            if (pid < 0)
            {
                perror("fork");
            }
            if (pid == 0)
            {
                execlp("rm", "rm", "players.zip", NULL);
            }
            else
            {
                // delete non MU
                waitpid(pid, &status, 0);
                pid = fork();

                if (pid < 0)
                {
                    perror("fork");
                }
                if (pid == 0)
                {
                    chdir("players");
                    execlp("find", "find", ".", "-type", "f", "-not", "-name", "*ManUtd*", "-delete", NULL);
                }
                else
                {
                    // group players dir
                    waitpid(pid, &status, 0);
                    pid = fork();

                    if (pid < 0)
                    {
                        perror("fork");
                    }
                    if (pid == 0)
                    {
                        chdir("players");
                        execlp("mkdir", "mkdir", "Bek", "Gelandang", "Kiper", "Penyerang", NULL);
                    }
                    else
                    {
                        // move players
                        waitpid(pid, &status, 0);
                        int i;
                        pid_t mpid[4];

                        // create four child processes
                        for (i = 0; i < 4; i++)
                        {
                            mpid[i] = fork();
                            if (mpid[i] < 0)
                            {
                                // fork failed
                                perror("fork");
                            }

                            if (mpid[i] == 0)
                            {
                                // child process
                                switch (i)
                                {
                                case 0:
                                    chdir("players");
                                    execlp("find", "find", ".", "-type", "f", "-name", "*Kiper_*", "-exec", "mv", "{}", "Kiper/", ";", NULL);
                                    break;
                                case 1:
                                    chdir("players");
                                    execlp("find", "find", ".", "-type", "f", "-name", "*Bek_*", "-exec", "mv", "{}", "Bek/", ";", NULL);
                                    break;
                                case 2:
                                    chdir("players");
                                    execlp("find", "find", ".", "-type", "f", "-name", "*Gelandang_*", "-exec", "mv", "{}", "Gelandang/", ";", NULL);
                                    break;
                                case 3:
                                    chdir("players");
                                    execlp("find", "find", ".", "-type", "f", "-name", "*Penyerang_*", "-exec", "mv", "{}", "Penyerang/", ";", NULL);
                                    break;
                                }
                            }
                        }
                        for (i = 0; i < 4; i++)
                        {
                            waitpid(mpid[i], NULL, 0);
                        }

                        buatTim(4, 3, 3);
                    }
                }
            }
        }
    }

    return 0;
}
```
**Screenshot Output Program Nomor 3 :**
| <p align="center"> Screenshot </p> | <p align="center"> Deskripsi </p> |
| ---------------------------------- | --------------------------------- |
|<img src="/uploads/d6b59d2f697d4645d8da366b538bbce0/no3.png" width = "350"/> | <p align="center"> Direktori setiap posisi pemain</p> |
|<img src="/uploads/98934b9ce89334b89bd4fd23ff8014e3/no3b.png" width = "350"/> | <p align="center"> Direktori Bek</p> |
|<img src="/uploads/94257e46849c1ddb4fe80f31d504e04f/no3c.png" width = "350"/> | <p align="center"> Direktori Gelandang</p> |
|<img src="/uploads/704a90d6a776a9ce194473277f2a18a4/no3d.png" width = "350"/> | <p align="center"> Direktori Kiper</p> |
|<img src="/uploads/f297969d5d6b099d2cb90d2641e042a6/no3e.png" width = "350"/> | <p align="center"> Direktori Penyerang</p> |
|<img src="/uploads/24923e43e836d6b34f631a126d2914dd/no3f.png" width = "350"/> | <p align="center"> File formasi .txt yang berada pada /home/arvianta</p> |


## Nomor 4
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat **program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C** karena baru dipelajari olehnya.

Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa **Jam** (0-23), **Menit** (0-59), **Detik** (0-59), **Tanda asterisk** [ * ] (value bebas), serta **path file .sh**.
- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan **pesan “error”** apabila argumen yang diterima program tidak sesuai. **Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.**
- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini **berjalan dalam background** dan **hanya menerima satu config cron.**
- Bonus poin apabila CPU state minimum.

**Contoh untuk run**: /program \* 44 5 /home/Banabil/programcron.sh

**Solusi:**
```c
void run_script(int hr, int min, int sec, char* script_path) {
    time_t current_time;
    struct tm *local_time;

    current_time = time(NULL);
    local_time = localtime(&current_time);

    if ((hr == -1 || hr == local_time->tm_hour) && (min == -1 || min == local_time->tm_min) && (sec == -1 || sec == local_time->tm_sec)){
        pid_t pid = fork();
        if (pid == 0){
            execl("/bin/bash", "/bin/bash", script_path, NULL);
        }
    }
}
```

- **Pertama**, membuat fungsi dengan 4 parameter: ``hr`` untuk jam, ``min`` untuk menit, ``sec`` untuk detik, dan ``char* script_path`` sebagai string path. 

    Fungsi ``time()`` akan mendefinisikan waktu saat ini dan akan diubah menjadi waktu lokal menggunakan fungsi ``localtime()``. Jika waktu saat ini sesuai dengan waktu yang diinput, maka ``fork()`` akan membuat proses/child baru dan dijalankan oleh ``execl()``.



```c
int main(int argc, char *argv[])
```

- **Kedua**, inisialisasi _main function._

```c
if (argc != 5) {
    fprintf(stderr, "Error: Expected 5 arguments, received %d\n", argc);
    exit(EXIT_FAILURE);
}
```
- **Ketiga**, mendefinisikan kondisi yang mana jika input tidak sama dengan 5, maka program akan mencetak **_Error: Expected 5 arguments_** yang menandakan error.

```c
int hr, min, sec;
```

- **Keempat**, Inisialisasi variabel ``hr`` sebagai jam, ``min`` sebagai menit, dan ``sec`` sebagai detik.

```c
if (strcmp(argv[1], "*") == 0) {
    hr = -1;
} else {
    hr = atoi(argv[1]);
    if (hr < 0 || hr > 23) {
        fprintf(stderr, "Error: Hour argument '%s' must be an integer between 0 and 23\n", argv[1]);
        exit(EXIT_FAILURE);
    }
}
```
- **Kelima**, jika argumen pertama sama dengan karakter asterisk "*", maka ``hr`` akan bernilai ``-1``. jika tidak, maka program akan menggunakan ``atoi(argv[1])`` untuk mengubah string menjadi integer dan menyimpan nilai pada ``hr``.

    Lalu jika ``hr`` kurang dari 0 atau lebih dari 23, maka program akan mencetak error.


```c
if (strcmp(argv[2], "*") == 0) {
    min = -1;
} else {
    min = atoi(argv[2]);
    if (min < 0 || min > 59) {
        fprintf(stderr, "Error: Minute argument '%s' must be an integer between 0 and 59\n", argv[2]);
        exit(EXIT_FAILURE);
    }
}
```
- **Keenam**, jika argumen pertama sama dengan karakter asterisk "*", maka ``min`` akan bernilai ``-1``. jika tidak, maka program akan menggunakan ``atoi(argv[2])`` untuk mengubah string menjadi integer dan menyimpan nilai pada ``min``.

    Lalu, jika ``min`` kurang dari 0 atau lebih dari 59, maka program akan mencetak error.


```c
if (strcmp(argv[3], "*") == 0) {
        sec = -1;
} else {
    sec = atoi(argv[3]);
    if (sec < 0 || sec > 59) {
        fprintf(stderr, "Error: Second argument '%s' must be an integer between 0 and 59\n", argv[3]);
        exit(EXIT_FAILURE);
    }
}
```
- **Ketujuh**, jika argumen pertama sama dengan karakter asterisk "*", maka ``sec`` akan bernilai ``-1``. jika tidak, maka program akan menggunakan ``atoi(argv[3])`` untuk mengubah string menjadi integer dan menyimpan nilai pada ``sec``.

    Lalu, jika ``sec`` kurang dari 0 atau lebih dari 59, maka program akan mencetak error.


```c
if (access(argv[4], F_OK) == -1) {
    fprintf(stderr, "Error: Script file '%s' does not exist\n", argv[4]);
    exit(EXIT_FAILURE);
} else if (access(argv[4], X_OK) == -1) {
    fprintf(stderr, "Error: Script file '%s' is not executable\n", argv[4]);
    exit(EXIT_FAILURE);
}
```
- **Kedelapan**, ``access(argv[4])`` berfungsi untuk memeriksa apakah file dapat diakses atau tidak, dengan menggunakan ``F_OK``. Jika nilai yang di-return adalah ``-1``, maka file tersebut tidak dapat diakses dan mencetak error.

    Jika file dapat diakses, maka program akan memeriksa apakah file dapat dieksekusi atau tidak dengan menggunakan ``X_OK``. Jika nilai yang di-return adalah ``-1``, maka file tidak dapat dijalankan dan mencetak error.


```c
pid_t pid = fork();

    if (pid < 0){
        printf("Error: Fork failed.\n");
        exit(EXIT_FAILURE);
    }
    if (pid > 0){
        exit(EXIT_SUCCESS);
    }
```

- **Selanjutnya**, ``fork()`` di sini bertujuan untuk membuat _child_ baru. Jika nilai yang di-return kurang dari 0, itu menandakan gagal dalam membuat _child_ baru dan program akan mencetak error.

    Jika proses pembuatan _child_ berhasil, maka ``fork()`` akan me-return 0. Karena nilai ``pid`` tidak sama dengan 0, menandai bahwa program berjalan pada ``parent``. Pada proses ``parent``, program langsung keluar dengan status ``('EXIT_SUCCESS)``.


```c
umask(0);
pid_t sid = setsid();
```

- Sesuai namanya, yaitu ``umask(0)``, bertujuan untuk mengatur masker pembuatan mode file menjadi 0.

    ``pid_t sid = setsid()`` membuat sebuah sesi baru dan memperoleh ID yang akan digunakan sebagai ID proses. ``setsid()`` akan membuat proses menjadi _session leader_ dan me-return ID baru.



```c
if (sid < 0){
    printf("Error: Could not create new session.\n");
    exit(EXIT_FAILURE);
}
```

- **Berikutnya**, apabila ``setsid()`` me-return nilai kurang dari 0, ini menunjukkan bahwa tidak dapat membuat tahap/sesi baru. Lalu, program akan mencetak error.

    Hal ini menunjukkan bahwa ada kesalahan dalam memulai daemon.


```c
if ((chdir("/")) < 0){
    printf("Error: Could not change working directory to root.\n");
    exit(EXIT_FAILURE);
}
```

- Apabila perubahan direktori kerja ke direktori root gagal dan ``chdir()`` me-return nilai kurang dari 0, maka program  akan mencetak error.

    Hal ini menunjukkan bahwa ada kesalahan dalam pengaturan direktori kerja daemon ke direktori root.



```c
close(STDIN_FILENO);
close(STDOUT_FILENO);
close(STDERR_FILENO);
```

- **STDIN_FILENO** merupakan file descriptor untuk input.
- **STDOUT_FILENO** merupakan file descriptor untuk output.
- **STDERR_FILENO** merupakan file descriptor untuk error output.



```c
while(1) {
    run_script(hr, min, sec, argv[4]);
    sleep(1);
}
```

- **Terakhir**, digunakan iterasi ``while()`` yang mana setiap 1 detik, fungsi ``run_script(hr, min, sec, argv[4])`` akan dipanggil dengan parameter yang sudah didefinisikan di awal. Jika waktu saat ini sama dengan waktu yang telah diatur, maka ``fork()`` akan digunakan untuk membuat tahap/sesi baru. Proses ini akan menjalankan script menggunakan ``excl()``. 

    Fungsi ``sleep(1)`` bertujuan untuk menunggu selama 1 detik sebelum mengulangi iterasi.

**Jawaban:**

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

// Danno Denis Dhaifullah 5025211027 && Rayhan Arvianta Bayuputra 5025211217

void run_script(int hr, int min, int sec, char* script_path)
{
    time_t current_time;
    struct tm *local_time;

    current_time = time(NULL);
    local_time = localtime(&current_time);

    if ((hr == -1 || hr == local_time->tm_hour) && (min == -1 || min == local_time->tm_min) && (sec == -1 || sec == local_time->tm_sec))
    {
        pid_t pid = fork();
        if (pid == 0)
        {
            execl("/bin/bash", "/bin/bash", script_path, NULL);
        }
    }
}

int main(int argc, char *argv[])
{
    if (argc != 5) {
        fprintf(stderr, "Error: Expected 5 arguments, received %d\n", argc);
        exit(EXIT_FAILURE);
    }

    int hr, min, sec;

    if (strcmp(argv[1], "*") == 0) {
        hr = -1;
    } else {
        hr = atoi(argv[1]);
        if (hr < 0 || hr > 23) {
            fprintf(stderr, "Error: Hour argument '%s' must be an integer between 0 and 23\n", argv[1]);
            exit(EXIT_FAILURE);
        }
    }

    if (strcmp(argv[2], "*") == 0) {
        min = -1;
    } else {
        min = atoi(argv[2]);
        if (min < 0 || min > 59) {
            fprintf(stderr, "Error: Minute argument '%s' must be an integer between 0 and 59\n", argv[2]);
            exit(EXIT_FAILURE);
        }
    }

    if (strcmp(argv[3], "*") == 0) {
        sec = -1;
    } else {
        sec = atoi(argv[3]);
        if (sec < 0 || sec > 59) {
            fprintf(stderr, "Error: Second argument '%s' must be an integer between 0 and 59\n", argv[3]);
            exit(EXIT_FAILURE);
        }
    }

    if (access(argv[4], F_OK) == -1) {
        fprintf(stderr, "Error: Script file '%s' does not exist\n", argv[4]);
        exit(EXIT_FAILURE);
    } else if (access(argv[4], X_OK) == -1) {
        fprintf(stderr, "Error: Script file '%s' is not executable\n", argv[4]);
        exit(EXIT_FAILURE);
    }

    pid_t pid = fork();
    if (pid < 0)
    {
        printf("Error: Fork failed.\n");
        exit(EXIT_FAILURE);
    }
    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    pid_t sid = setsid();
    if (sid < 0)
    {
        printf("Error: Could not create new session.\n");
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0)
    {
        printf("Error: Could not change working directory to root.\n");
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1)
    {
        run_script(hr, min, sec, argv[4]);

        sleep(1);
    }

    return 0;
}
```

**Screenshot Output Program Nomor 4 :**
| <p align="center"> Screenshot </p> | <p align="center"> Deskripsi </p> |
| ---------------------------------- | --------------------------------- |
|<img src="/uploads/28cd6f02b02c21bb75a0053d3a2fda7e/no4.png" width = "350"/> | <p align="center"> Pemanggilan simulasi crontab untuk bash script ``test.sh``</p> |
|<img src="/uploads/7d3aa7d32afaf80d84443fa5c1e5f8a6/no4b.png" width = "350"/> | <p align="center"> Proses berjalan sesuai permintaan</p> |

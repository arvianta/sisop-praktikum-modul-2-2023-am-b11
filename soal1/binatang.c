#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <dirent.h>

// Rayhan Arvianta Bayuputra 5025211217

void randomSelect() {
    char *picture_folder = "/home/arvianta/sisop/P2/NO1";
    char command[1000];
    sprintf(command, "/bin/bash -c '"
                    "picture_folder=\"%s\" && "
                    "picture_files=($picture_folder/*.jpg) && "
                    "if [ ${#picture_files[@]} -eq 0 ]; then "
                    "echo \"No .jpg files found in the folder\" && exit 1; "
                    "fi && "
                    "random_index=$((RANDOM %% ${#picture_files[@]})) && "
                    "random_picture=\"$(basename ${picture_files[$random_index]} .jpg)\" && "
                    "echo \"Pada shift kali ini, Grape-Kun menjaga $random_picture\"'"
                    , picture_folder);
    system(command);
}

void movePictures() {
    char *picture_folder = "/home/arvianta/sisop/P2/NO1";
    char command[2000];
    sprintf(command, "/bin/bash -c '"
            "# get the picture folder from the first argument\n"
            "picture_folder=\"%s\"\n"
            "\n"
            "# create directories for each type of animal\n"
            "mkdir -p \"$picture_folder/HewanDarat\"\n"
            "mkdir -p \"$picture_folder/HewanAmphibi\"\n"
            "mkdir -p \"$picture_folder/HewanAir\"\n"
            "\n"
            "# loop through picture files in folder\n"
            "for file in \"$picture_folder\"/*; do\n"
            "  # get the filename without path and extension\n"
            "  filename=$(basename -- \"$file\")\n"
            "  extension=\"${filename##*.}\"\n"
            "  filename=\"${filename%%.*}\"\n"
            "  \n"
            "  # get the last name of the animal\n"
            "  last_name=\"${filename##*_}\"\n"
            "  \n"
            "  # move the file to the appropriate directory based on last name\n"
            "  if [[ \"$last_name\" == \"darat\" ]]; then\n"
            "    mv \"$file\" \"$picture_folder/HewanDarat/$filename.$extension\"\n"
            "  elif [[ \"$last_name\" == \"amphibi\" ]]; then\n"
            "    mv \"$file\" \"$picture_folder/HewanAmphibi/$filename.$extension\"\n"
            "  elif [[ \"$last_name\" == \"air\" ]]; then\n"
            "    mv \"$file\" \"$picture_folder/HewanAir/$filename.$extension\"\n"
            "  fi\n"
            "done'"
            , picture_folder);
    system(command);
}

void deleteFolders() {
    char command[1000];
    
    // delete the HewanAir, HewanAmphibi, and HewanDarat folders
    sprintf(command, "rm -r HewanAir HewanAmphibi HewanDarat");
    system(command);

    // delete the binatang.zip file
    sprintf(command, "rm binatang.zip");
    system(command);
}

int main()
{    
    pid_t pid;
    int status;
    pid = fork();

    if (pid < 0) {
        perror("fork");
    }
    if(pid == 0) {
        execlp("wget", "wget", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL);
        exit(0);
    } else {
        // unzip
        waitpid(pid, &status, 0);
        pid = fork();
        
        if (pid < 0) {
            perror("fork");
        }
        if (pid == 0) {
            execlp("unzip", "unzip", "binatang.zip", "-d", "/home/arvianta/sisop/P2/NO1", NULL);
            // char *argv[] = {"unzip", "binatang.zip", "-d", "/home/arvianta/sisop/P2/NO1", NULL};
            // execv("/bin/unzip", argv);
        } else {
            // random select
            waitpid(pid, &status, 0);
            pid = fork();

            if (pid < 0) {
                perror("fork");
            }

            if (pid == 0) {
                randomSelect();
            }
            else {
                // move pictures
                waitpid(pid, &status, 0);
                pid = fork();

                if (pid < 0) {
                    perror("fork");
                }

                if (pid == 0) {
                    movePictures();
                }

                else {
                    // zip folder hewan air
                    waitpid(pid, &status, 0);
                    pid = fork();

                    if (pid < 0) {
                        perror("fork");
                    }

                    if (pid == 0) {
                        execlp("zip", "zip", "-r", "HewanAir.zip", "HewanAir", NULL);
                    }

                    else {
                        // zip folder hewan amphibi
                        waitpid(pid, &status, 0);
                        pid = fork();

                        if (pid < 0) {
                            perror("fork");
                        }

                        if (pid == 0) {
                            execlp("zip", "zip", "-r", "HewanAmphibi.zip", "HewanAmphibi", NULL);
                        }

                        else {
                            // zip folder hewan darat
                            waitpid(pid, &status, 0);
                            pid = fork();

                            if (pid < 0) {
                                perror("fork");
                            }

                            if (pid == 0) {
                                execlp("zip", "zip", "-r", "HewanDarat.zip", "HewanDarat", NULL);
                            }

                            else {
                                // delete unnecessary files/folders
                                waitpid(pid, &status, 0);
                                pid = fork();

                                if (pid < 0) {
                                    perror("fork");
                                }

                                if (pid == 0) {
                                    deleteFolders();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return 0;
}

//Danno Denis Dhaifullah 5025211027 && Rayhan Arvianta Bayuputra 5025211217

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>
#include<signal.h>
#include<unistd.h>
#include<syslog.h>
#include<fcntl.h>
#include<errno.h>
#include<time.h>
#include<wait.h>
#include<sys/prctl.h>
#include<sys/types.h>
#include<sys/stat.h>

//membuat killer.c sesuai dengan ketentuan mode a dan b
void killer(bool mode, pid_t sid){
	
	//akses write ke file
    FILE *filekill = fopen("killer.c", "w+");
    
    //header killer.c
    fprintf(filekill, "#include <stdio.h>\n");
    fprintf(filekill, "#include <stdlib.h>\n");
    fprintf(filekill, "#include <unistd.h>\n");
    fprintf(filekill, "#include <wait.h>\n");
    
    //main func
    fprintf(filekill, "int main() {\n");

    //fork dahulu untuk melihat proses, apabila 0 cek argumen mode
    fprintf(filekill, "pid_t child_id = fork();\n");
    fprintf(filekill, "if (child_id == 0) {\n");
    
    //MODE_A
    char argmode[100];
    if (mode == true) {
        fprintf(filekill, "execl(\"/usr/bin/pkill\", \"pkill\", \"-9\", \"-s\", \"%d\", NULL);\n", sid);
    }
    
    //MODE_B
    else if (mode == false) {
        fprintf(filekill, "execl(\"/bin/kill\", \"kill\", \"-9\", \"%d\", NULL);\n", getpid());        
    }
    
    fprintf(filekill, "}\n");
    fprintf(filekill, "while(wait(NULL) > 0);\n");
    fprintf(filekill, "execl(\"/usr/bin/rm\", \"rm\", \"killer\", NULL);\n");
    fprintf(filekill, "return 0; }\n");

    fclose(filekill);
	
	int flag = 0;

    // compile killer.c
    pid_t child_id = fork();
    if(child_id == 0){
        execl("/usr/bin/gcc", "gcc", "killer.c", "-o", "killer", NULL);
    }
	waitpid(child_id, &flag, 0);

    // hapus killer.c
    child_id = fork();
    if (child_id == 0) {
    	execl("/usr/bin/rm", "rm", "killer.c", NULL);
    }
    
    //setelah dicompile akan membentuk file killer, lalu file killer.c dihapus.
}

int main(int argc, char* argv[]){
    // Check argument
	bool mode;
	if(argc == 1){
		printf("Use argument -a or -b\n");
		exit(0);
	}
    if(strcmp("-a", argv[1]) == 0) {
    	printf("MODE_A\n");
        mode = true;
    } 
	else if (strcmp("-b", argv[1]) == 0) {
		printf("MODE_B\n");
        mode = false;
    } 
	else {
        printf("Inputkan the right mode (-a or -b)\n");
        exit(1);
    }
    
	pid_t pid, sid; // Variabel untuk menyimpan PID
	pid = fork();   // Menyimpan PID dari child

	/* Keluar saat fork gagal (nilai variabel pid < 0) */
	if (pid < 0) {
		exit(EXIT_FAILURE);
  	}

	if (pid > 0) {
	   exit(EXIT_SUCCESS);
	}

	umask(0);

  	sid = setsid();
  	if (sid < 0) {
    	exit(EXIT_FAILURE);
  	}
  	
	//check program mode
	killer(mode, sid);
  	close(STDIN_FILENO); //input
  	close(STDOUT_FILENO); //output
  	close(STDERR_FILENO); //error

    while(1){
        char foldernm[20];
		time_t ctime = time(NULL);
		struct tm* ttime = localtime(&ctime);
	    strftime(foldernm, sizeof(foldernm), "%Y-%m-%d_%H:%M:%S", ttime);
       
        pid_t pid = fork();
	    if(pid == 0){
	    	//membuat folder
	    	mkdir(foldernm, 0777);
	    	
	    	//download gambar
	    	pid_t down;
		    char imagenm[25], 
				 loc[50], 
				 link[50];
		   
		   	//loop 15x, untuk mendapat 15 gambar setiap foldernya. 
		    for(int i = 0; i < 15; i++){
		        down = fork();
		        if(down == 0){
		            time_t ctime2 = time(NULL);
		            struct tm* ttime2 = localtime(&ctime2);
		            strftime(imagenm, sizeof(imagenm), "%Y-%m-%d_%H:%M:%S.jpg", ttime2);
		            sprintf(loc, "%s/%s", foldernm, imagenm);
		            sprintf(link, "https://picsum.photos/%ld", (ctime2 % 1000) + 50);		            
		            execl("/usr/bin/wget", "wget", "-q", "-O", loc, link, NULL);
		        }
		        sleep(5);
		    }
		    while(wait(NULL) > 0);
		    
		    //zip folder
		    pid_t zip = fork();
		    if(zip == 0) {
                execl("/usr/bin/zip", "zip", "-r", foldernm, foldernm, NULL);
            }
            int statuszip = 0;
		    waitpid(zip, &statuszip, 0);
            execl("/usr/bin/rm", "rm", "-r", foldernm, NULL);
		}
		
        sleep(30);
    }
}

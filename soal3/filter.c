#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>

// Rayhan Arvianta Bayuputra 5025211217

typedef struct
{
    int rating;
    char filename[300];
} Player;

int compareRate(const void *a, const void *b)
{
    const Player *player_a = (const Player *)a;
    const Player *player_b = (const Player *)b;
    return player_b->rating - player_a->rating;
}

void getPlayerRating(const char *position, Player *players, int *count)
{
    struct dirent *entry;
    DIR *dir;
    char directory[100];

    snprintf(directory, 100, "./players/%s", position);
    dir = opendir(directory);

    if (dir)
    {
        for (entry = readdir(dir); entry; entry = readdir(dir))
        {
            if (strstr(entry->d_name, ".png") && entry->d_type == DT_REG)
            {
                int rating;
                sscanf(entry->d_name, "%*[^_]_%*[^_]_%*[^_]_%d.png", &rating);
                strcpy(players[*count].filename, entry->d_name);
                players[*count].rating = rating;
                (*count)++;
            }
        }
        closedir(dir);
    }
    else
    {
        exit(EXIT_FAILURE);
    }
}

void buatTim(int bek, int gelandang, int penyerang)
{
    Player pemain_gelandang[100], pemain_penyerang[100], kiper[1], pemain_bek[100];
    int jumlah_gelandang = 0, jumlah_penyerang = 0, jumlah_kiper = 0, jumlah_bek = 0;

    int total_players = bek + gelandang + penyerang + 1;
    if (total_players != 11) {
        printf("There must be exactly 11 players in the team! With your formation, there are %d players.\n", total_players);
        return;
    } 

    getPlayerRating("Bek", pemain_bek, &jumlah_bek);
    if (jumlah_bek < bek) {
        printf("Insufficient bek players in the team.\n");
        return;
    }

    getPlayerRating("Gelandang", pemain_gelandang, &jumlah_gelandang);
    if (jumlah_gelandang < gelandang) {
        printf("Insufficient gelandang players in the team.\n");
        return;
    }

    getPlayerRating("Penyerang", pemain_penyerang, &jumlah_penyerang);
    if (jumlah_penyerang < penyerang) {
        printf("Insufficient penyerang players in the team.\n");
        return;
    }

    getPlayerRating("Kiper", kiper, &jumlah_kiper);

    qsort(pemain_gelandang, jumlah_gelandang, sizeof(Player), compareRate);
    qsort(pemain_penyerang, jumlah_penyerang, sizeof(Player), compareRate);
    qsort(pemain_bek, jumlah_bek, sizeof(Player), compareRate);

    char filepath[300];
    snprintf(filepath, 300, "/home/arvianta/Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
    FILE *file = fopen(filepath, "w");

    if (file)
    {
        int i;
        fprintf(file, "=====Kiper=====\n%s\n\n", kiper[0].filename);

        fprintf(file, "=====Bek=====\n");
        for (i = 0; i < bek && i < jumlah_bek; i++)
        {
            fprintf(file, "%d. %s\n", i + 1, pemain_bek[i].filename);
        }

        fprintf(file, "\n=====Gelandang=====\n");
        for (i = 0; i < gelandang && i < jumlah_gelandang; i++)
        {
            fprintf(file, "%d. %s\n", i + 1, pemain_gelandang[i].filename);
        }

        fprintf(file, "\n=====Penyerang=====\n");
        for (i = 0; i < penyerang && i < jumlah_penyerang; i++)
        {
            fprintf(file, "%d. %s\n", i + 1, pemain_penyerang[i].filename);
        }

        fclose(file);
        printf("Lineup saved\n");
    }
    else
    {
        printf("Error\n");
        exit(EXIT_FAILURE);
    }
}

int main()
{
    pid_t pid;
    int status;
    pid = fork();

    if (pid < 0)
    {
        perror("fork");
    }
    if (pid == 0)
    {
        execlp("wget", "wget", "-O", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL);
        exit(0);
    }
    else
    {
        // unzip
        waitpid(pid, &status, 0);
        pid = fork();

        if (pid < 0)
        {
            perror("fork");
        }
        if (pid == 0)
        {
            execlp("unzip", "unzip", "players.zip", NULL);
        }
        else
        {
            // delete players.zip
            waitpid(pid, &status, 0);
            pid = fork();
            if (pid < 0)
            {
                perror("fork");
            }
            if (pid == 0)
            {
                execlp("rm", "rm", "players.zip", NULL);
            }
            else
            {
                // delete non MU
                waitpid(pid, &status, 0);
                pid = fork();

                if (pid < 0)
                {
                    perror("fork");
                }
                if (pid == 0)
                {
                    chdir("players");
                    execlp("find", "find", ".", "-type", "f", "-not", "-name", "*ManUtd*", "-delete", NULL);
                }
                else
                {
                    // group players dir
                    waitpid(pid, &status, 0);
                    pid = fork();

                    if (pid < 0)
                    {
                        perror("fork");
                    }
                    if (pid == 0)
                    {
                        chdir("players");
                        execlp("mkdir", "mkdir", "Bek", "Gelandang", "Kiper", "Penyerang", NULL);
                    }
                    else
                    {
                        // move players
                        waitpid(pid, &status, 0);
                        int i;
                        pid_t mpid[4];

                        // create four child processes
                        for (i = 0; i < 4; i++)
                        {
                            mpid[i] = fork();
                            if (mpid[i] < 0)
                            {
                                // fork failed
                                perror("fork");
                            }

                            if (mpid[i] == 0)
                            {
                                // child process
                                switch (i)
                                {
                                case 0:
                                    chdir("players");
                                    execlp("find", "find", ".", "-type", "f", "-name", "*Kiper_*", "-exec", "mv", "{}", "Kiper/", ";", NULL);
                                    break;
                                case 1:
                                    chdir("players");
                                    execlp("find", "find", ".", "-type", "f", "-name", "*Bek_*", "-exec", "mv", "{}", "Bek/", ";", NULL);
                                    break;
                                case 2:
                                    chdir("players");
                                    execlp("find", "find", ".", "-type", "f", "-name", "*Gelandang_*", "-exec", "mv", "{}", "Gelandang/", ";", NULL);
                                    break;
                                case 3:
                                    chdir("players");
                                    execlp("find", "find", ".", "-type", "f", "-name", "*Penyerang_*", "-exec", "mv", "{}", "Penyerang/", ";", NULL);
                                    break;
                                }
                            }
                        }
                        for (i = 0; i < 4; i++)
                        {
                            waitpid(mpid[i], NULL, 0);
                        }

                        buatTim(4, 3, 3);
                    }
                }
            }
        }
    }

    return 0;
}
